<?php

namespace Orbis\Account\Test;

use Orbis\Account\Auth;
use Orbis\Account\LoyaltyCard;

/**
 * LoyaltyCard
 */
class LoyaltyCardTest extends \PHPUnit_Framework_TestCase
{
    const email = 'superuser@superuser.com';
    const password = 'password';

    const ID = "jxlnqonrpk";
    const ORGANIZATION_ID = "qovelknzlx";

    public function __construct()
    {
        Auth::setCredentials(self::email, self::password);
    }

    public function testListAll()
    {
        Auth::setCredentials(self::email, self::password);
        $loyalty = LoyaltyCard::listAll();

        $this->assertNotNull($loyalty);
    }

    public function testCreate()
    {
        Auth::setCredentials(self::email, self::password);

        $random = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 10);
        $companion = LoyaltyCard::create(['account_type' => 'personal_account', 'account_id' => 'baknvlanrp', 'carrier' => $random, 'card_number' => $random]);

        file_put_contents('id.json', $companion->id);

        $this->assertNotNull($companion);
    }

    public function testShow()
    {
        Auth::setCredentials(self::email, self::password);

        $id = self::ID;

        $companion = LoyaltyCard::show($id);

        $this->assertEquals($id, $companion->id);
    }

    public function testUpdate()
    {
        Auth::setCredentials(self::email, self::password);

        $id = self::ID;

        $newCarrier = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 10);

        LoyaltyCard::update($id, ['carrier' => $newCarrier]);
        $companion = LoyaltyCard::Show($id);
        $this->assertEquals($id, $companion->id);
    }

    public function testDelete()
    {
        Auth::setCredentials(self::email, self::password);

        $id = file_get_contents('id.json');

        $returnObject = LoyaltyCard::delete($id);

        $this->assertNotNull($returnObject);
    }

}
