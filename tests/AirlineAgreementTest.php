<?php

namespace Orbis\Account\Test;

use Orbis\Account\AirlineAgreement;
use Orbis\Account\Auth;

/**
 * Airline Argeement
 */
class AirlineAgreementTest extends \PHPUnit_Framework_TestCase
{
    const email = 'superuser@superuser.com';
    const password = 'password';

    const ID = "pbmeaxnrqv";
    const ORGANIZATION_ID = "qovelknzlx";

    public function __construct()
    {
        Auth::setCredentials(self::email, self::password);
    }

    public function testListAll()
    {
        Auth::setCredentials(self::email, self::password);
        $argeements = AirlineAgreement::listAll();

        $this->assertNotNull($argeements);
    }

    public function testCreate()
    {
        Auth::setCredentials(self::email, self::password);

        $random = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 10);
        $argeement = AirlineAgreement::create(['organization_id' => self::ORGANIZATION_ID, 'agreement_code' => $random, 'carrier' => $random]);

        file_put_contents('id.json', $argeement->id);

        $this->assertNotNull($argeement);
    }

    public function testShow()
    {
        Auth::setCredentials(self::email, self::password);

        $id = self::ID;

        $argeement = AirlineAgreement::show($id);

        $this->assertEquals($id, $argeement->id);
    }

    public function testUpdate()
    {
        Auth::setCredentials(self::email, self::password);

        $id = self::ID;

        $newCarrier = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 10);

        AirlineAgreement::update($id, ['carrier' => $newCarrier]);
        $argeement = AirlineAgreement::Show($id);
        $this->assertEquals($id, $argeement->id);
    }

    public function testDelete()
    {
        Auth::setCredentials(self::email, self::password);

        $id = file_get_contents('id.json');

        $returnObject = AirlineAgreement::delete($id);

        $this->assertNotNull($returnObject);
    }

}
