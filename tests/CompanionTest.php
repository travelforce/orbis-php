<?php

namespace Orbis\Account\Test;

use Orbis\Account\Auth;
use Orbis\Account\Companion;

/**
 * Companion
 */
class CompanionTest extends \PHPUnit_Framework_TestCase
{
    const email = 'superuser@superuser.com';
    const password = 'password';

    const ID = "jxlnqonrpk";
    const ORGANIZATION_ID = "qovelknzlx";

    public function __construct()
    {
        Auth::setCredentials(self::email, self::password);
    }

    public function testListAll()
    {
        Auth::setCredentials(self::email, self::password);
        $companions = Companion::listAll();

        $this->assertNotNull($companions);
    }

    public function testCreate()
    {
        Auth::setCredentials(self::email, self::password);

        $random = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 10);
        $companion = Companion::create(['account_type' => 'personal_account', 'account_id' => 'baknvaerpv', 'firstname' => $random]);

        file_put_contents('id.json', $companion->id);

        $this->assertNotNull($companion);
    }

    public function testShow()
    {
        Auth::setCredentials(self::email, self::password);

        $id = self::ID;

        $companion = Companion::show($id);

        $this->assertEquals($id, $companion->id);
    }

    public function testUpdate()
    {
        Auth::setCredentials(self::email, self::password);

        $id = self::ID;

        $newFirstName = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 10);

        Companion::update($id, ['firstname' => $newFirstName]);
        $companion = Companion::Show($id);
        $this->assertEquals($id, $companion->id);
    }

    public function testDelete()
    {
        Auth::setCredentials(self::email, self::password);

        $id = file_get_contents('id.json');

        $returnObject = Companion::delete($id);

        $this->assertNotNull($returnObject);
    }

}
