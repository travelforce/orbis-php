<?php

namespace Orbis\Account\Test;

use Orbis\Account\Auth;
use Orbis\Account\User;

/**
 *
 *
 */

class UserTest extends \PHPUnit_Framework_TestCase
{

    const email = 'superuser@superuser.com';
    const password = 'password';

    public function __construct()
    {
        Auth::setCredentials(self::email, self::password);
    }

    public function testGetProfile()
    {
        Auth::setCredentials(self::email, self::password);

        $user = User::getProfile();

        $this->assertObjectHasAttribute('firstname', $user);

    }

    public function testUpdateProfile()
    {
        $newName = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 10);
        User::updateProfile(['firstname' => $newName]);

        $user = User::getProfile();

        $this->assertEquals($user->firstname, $newName);
    }

    public function testUpdateRole()
    {
        Auth::setCredentials('p_27@test.com', 'password');
        $currentUser = User::getProfile();

        Auth::setCredentials(self::email, self::password);
        $role = 'moderator';
        User::updateRole(['user_id' => $currentUser->id, 'role' => $role]);

        Auth::setCredentials('p_27@test.com', 'password');

        $user = User::getProfile();

        $this->assertEquals($user->role, $role);

        Auth::setCredentials('superuser@superuser.com', 'password');

        $role = 'personal_account';
        User::updateRole(['user_id' => $currentUser->id, 'role' => $role]);

    }

}
