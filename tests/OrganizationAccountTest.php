<?php

namespace Orbis\Account\Test;

use Orbis\Account\Auth;
use Orbis\Account\OrganizationAccount;

/**
 * Organization Account
 */
class OrganizationAccountTest extends \PHPUnit_Framework_TestCase
{

    const email = 'superuser@superuser.com';
    const password = 'password';

    const ID = 'vjoebmknba';

    public function testCreate()
    {
        Auth::setCredentials(self::email, self::password);

        $newEmail = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 10) . '@tf.dev';
        $account = OrganizationAccount::create(['organization_id' => 'pbmeapxnrq', 'firstname' => 'test test', 'email' => $newEmail, 'password' => 'password']);

        file_put_contents('id.json', $account->id);
        $this->assertNotNull($account);
    }

    public function testShow()
    {
        Auth::setCredentials(self::email, self::password);

        $id = self::ID;
        $account = OrganizationAccount::show($id);

        $this->assertEquals($id, $account->id);

    }

    public function testUpdate()
    {
        Auth::setCredentials(self::email, self::password);

        $id = self::ID;

        $newFirstName = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 10);

        $returnOject = OrganizationAccount::update($id, ['firstname' => $newFirstName]);
        $account = OrganizationAccount::Show($id);

        $this->assertEquals($newFirstName, $account->user->firstname);

    }

    public function testDelete()
    {
        Auth::setCredentials(self::email, self::password);

        $id = file_get_contents('id.json');

        $returnObject = OrganizationAccount::delete($id);

        $this->assertNotNull($returnObject);
    }

    public function testresetPassword()
    {
        Auth::setCredentials(self::email, self::password);
        $id = self::ID;
        $data = [];
        $returnObject = OrganizationAccount::resetPassword($id, $data);

        $this->assertNotNull($returnObject);

    }

    public function testUpdateRole()
    {
        Auth::setCredentials(self::email, self::password);
        $data = ['role' => 'organization_user'];

        $returnObject = OrganizationAccount::updateRole(self::ID, $data);

        $this->assertNotNull($returnObject);

    }

    public function testBookings()
    {
        Auth::setCredentials(self::email, self::password);

        $id = self::ID;
        $returnObject = OrganizationAccount::bookings($id);

        $this->assertNotNull($returnObject);
    }

    public function testAttachBooking()
    {
        Auth::setCredentials(self::email, self::password);

        $data = ['booking_id' => 'pbmeaxnrqv'];
        $id = self::ID;

        $returnObject = OrganizationAccount::attachBooking($id, $data);

        $this->assertNotNull($returnObject);

    }

    public function testDetachBooking()
    {
        Auth::setCredentials(self::email, self::password);

        $data = ['booking_id' => 'pbmeaxnrqv'];
        $id = self::ID;

        $returnObject = OrganizationAccount::detachBooking($id, $data);

        $this->assertNotNull($returnObject);

    }

    public function testCompanions()
    {
        Auth::setCredentials(self::email, self::password);
        $id = self::ID;

        $returnObject = OrganizationAccount::companions($id);

        $this->assertNotNull($returnObject);

    }

    public function testLoyaltyCards()
    {
        Auth::setCredentials(self::email, self::password);
        $id = self::ID;

        $returnObject = OrganizationAccount::loyaltycards($id);

        $this->assertNotNull($returnObject);
    }

}
