<?php

namespace Orbis\Account\Test;

use Orbis\Account\Auth;
use Orbis\Account\Organization;

/**
 * Organization
 */
class OrganizationTest extends \PHPUnit_Framework_TestCase
{

    const email = 'superuser@superuser.com';
    const password = 'password';
    const ID = 'pbmeapxnrq';

    public function __construct()
    {
        Auth::setCredentials(self::email, self::password);
    }

    public function testListAll()
    {
        Auth::setCredentials(self::email, self::password);

        $organizations = Organization::listAll();

        $this->assertNotNull($organizations);
    }

    public function testCreate()
    {
        Auth::setCredentials(self::email, self::password);

        $data = ['name' => 'Bhusal Company', 'address' => 'Kathmandu Nepal'];

        $organization = Organization::create($data);
        file_put_contents('id.json', $organization->id);
        $this->assertNotNull($organization);
    }

    public function testShow()
    {
        Auth::setCredentials(self::email, self::password);

        $id = self::ID;

        $organization = Organization::show($id);

        $this->assertEquals($id, $organization->id);

    }

    public function testUpdate()
    {
        Auth::setCredentials(self::email, self::password);

        $id = self::ID;

        $newName = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 10);

        Organization::update($id, ['name' => $newName]);
        $organization = Organization::Show($id);

        $this->assertEquals($newName, $organization->name);

    }

    public function testListUser()
    {
        Auth::setCredentials(self::email, self::password);

        $id = 'pbmeapxnrq';

        $users = Organization::listUser($id);

        $this->assertNotNull($users);

    }

    public function testDelete()
    {
        Auth::setCredentials(self::email, self::password);

        $id = file_get_contents('id.json');

        $returnOject = Organization::delete($id);

        $this->assertNotNull($returnOject);

    }

    public function testBookings()
    {
        Auth::setCredentials(self::email, self::password);

        $id = 'pbmeapxnrq';

        $returnOject = Organization::bookings($id);
        $this->assertNotNull($returnOject);

    }

    public function testShowActiveOrganization()
    {
        Auth::setCredentials('o_a_0@test.com', self::password);

        $returnOject = Organization::showActiveOrganization();
        $this->assertNotNull($returnOject);

    }

    public function testUpdateActiveOrganization()
    {
        Auth::setCredentials('o_a_0@test.com', self::password);

        $returnOject = Organization::updateActiveOrganization(['address' => 'ramdev']);
        $this->assertNotNull($returnOject);
    }

    public function testActiveOrganizationBookings()
    {
        Auth::setCredentials('o_a_0@test.com', self::password);

        $returnOject = Organization::activeOrganizationBookings();
        $this->assertNotNull($returnOject);
    }

    public function testActiveOrganizationBookingStatistics()
    {
        Auth::setCredentials('o_a_0@test.com', self::password);

        $returnOject = Organization::activeOrganizationBookingStatistics();
        $this->assertNotNull($returnOject);
    }

    public function testActiveOrganizationUsers($page = '')
    {
        Auth::setCredentials('o_a_0@test.com', self::password);

        $returnOject = Organization::activeOrganizationUsers();
        $this->assertNotNull($returnOject);
    }

}
