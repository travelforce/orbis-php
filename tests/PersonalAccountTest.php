<?php

namespace Orbis\Account\Test;

use Orbis\Account\Auth;
use Orbis\Account\PersonalAccount;
use Orbis\Account\User;

/**
 * Personal Account
 */
class PersonalAccountTest extends \PHPUnit_Framework_TestCase
{
    const email = 'superuser@superuser.com';
    const password = 'password';

    const ID = "mlxeppdeko";

    public function __construct()
    {
        Auth::setCredentials(self::email, self::password);
    }

    public function testListAll()
    {
        Auth::setCredentials(self::email, self::password);
        $accounts = PersonalAccount::listAll();

        $this->assertNotNull($accounts);
    }

    public function testCreate()
    {
        Auth::setCredentials(self::email, self::password);

        $newEmail = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 10) . '@tf.dev';
        $account = PersonalAccount::create(['firstname' => 'test test', 'email' => $newEmail, 'password' => 'password']);

        file_put_contents('id.json', $account->id);

        $this->assertNotNull($account);
    }

    public function testShow()
    {
        Auth::setCredentials(self::email, self::password);

        $id = self::ID;

        $account = PersonalAccount::show($id);

        $this->assertEquals($id, $account->id);
    }

    public function testUpdate()
    {
        Auth::setCredentials(self::email, self::password);

        $id = self::ID;

        $newFirstName = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 10);

        PersonalAccount::update($id, ['firstname' => $newFirstName]);
        $account = PersonalAccount::Show($id);
        $this->assertEquals($newFirstName, $account->user->firstname);
    }

    public function testDelete()
    {
        Auth::setCredentials(self::email, self::password);

        $id = file_get_contents('id.json');

        $returnObject = PersonalAccount::delete($id);

        $this->assertNotNull($returnObject);
    }

    public function testBookings()
    {
        Auth::setCredentials(self::email, self::password);

        $returnObject = PersonalAccount::bookings(self::ID);

        $this->assertNotNull($returnObject);
    }

    public function testBookingStatistics()
    {
        Auth::setCredentials(self::email, self::password);

        $returnObject = PersonalAccount::bookingStatistics(self::ID);

        $this->assertNotNull($returnObject);
    }

    public function testAttachBooking()
    {
        Auth::setCredentials(self::email, self::password);

        $data = ['booking_id' => 'pbmeaxnrqv'];
        $id = self::ID;

        $returnObject = PersonalAccount::attachBooking($id, $data);

        $this->assertNotNull($returnObject);

    }

    public function testDetachBooking()
    {
        Auth::setCredentials(self::email, self::password);

        $data = ['booking_id' => 'pbmeaxnrqv'];
        $id = self::ID;

        $returnObject = PersonalAccount::detachBooking($id, $data);

        $this->assertNotNull($returnObject);

    }

    public function testCompanions()
    {
        Auth::setCredentials(self::email, self::password);
        $id = self::ID;

        $returnObject = PersonalAccount::companions($id);

        $this->assertNotNull($returnObject);

    }

    public function testLoyaltyCards()
    {
        Auth::setCredentials(self::email, self::password);
        $id = self::ID;

        $returnObject = PersonalAccount::loyaltycards($id);

        $this->assertNotNull($returnObject);

    }

}
