<?php

namespace Orbis\Constants;

class Api
{
    private static $baseUrl     = 'https://api.umeaflyg.se';
    private static $timeout = 200.0;

    public static function getBaseUrl()
    {
        return self::$baseUrl;
    }

    public static function setBaseUrl($baseUrl)
    {
        self::$baseUrl = $baseUrl;
    }

    public static function getTimeout()
    {
        return self::$timeout;
    }

    public static function setTimeout($timeout)
    {
        self::$timeout = $timeout;
    }
}
