<?php

namespace Orbis\Connection\Contracts;

use Psr\Http\Message\ResponseInterface;

interface RestClientInterface
{
    public function get($endpoint, $query = '');
    public function post($endpoint, $data);
    public function put($endpoint, $data);
    public function delete($endpoint);
    public function responseHandler(ResponseInterface $response);
}
