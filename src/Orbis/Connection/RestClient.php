<?php

namespace Orbis\Connection;

use GuzzleHttp\Client;
use Orbis\Connection\Contracts\RestClientInterface;
use Orbis\Connection\Exceptions\FormValidationError;
use Orbis\Connection\Exceptions\GenericHTTPError;
use Orbis\Connection\Exceptions\InvalidCredentials;
use Orbis\Connection\Exceptions\MissingEndpoint;
use Orbis\Connection\Exceptions\MissingRequiredParameters;
use Orbis\Connection\Exceptions\Unauthorized;
use Orbis\Constants\Api;
use Orbis\Constants\ExceptionMessages;
use Psr\Http\Message\ResponseInterface;

/**
 * This is restclient
 */
class RestClient implements RestClientInterface
{

    private $client;
    private $apiUrl;

    public function __construct()
    {
        $this->client = $client = new Client([
            // Base URL is used with relative requests
            'base_uri' => Api::getBaseUrl(),
            // You can set any number of default request options.
            'timeout' => Api::getTimeout(),

            'http_errors' => false,
        ]);
    }

    public function request($method, $endpoint, $data = [])
    {
        if (isset($_SESSION['auth_token'])) {
            $data['headers']['Authorization'] = 'Bearer ' . $_SESSION['auth_token'];
        }

        $response = $this->client->request($method, $endpoint, $data);

        return $this->responseHandler($response);
    }
    public function get($endpoint, $query = '')
    {

        return $response = $this->request('GET', $endpoint, ['query' => $query]);
    }

    public function post($endpoint, $data = [])
    {

        return $response = $this->request('POST', $endpoint, ['form_params' => $data]);
    }

    public function put($endpoint, $data)
    {
        return $response = $this->request('PUT', $endpoint, ['form_params' => $data]);
    }

    public function delete($endpoint)
    {

        return $response = $this->request('DELETE', $endpoint);
    }

    public function responseHandler(ResponseInterface $responseObj)
    {
        $httpResponseCode = $responseObj->getStatusCode();
        if ($httpResponseCode === 200) {

            $data = (string) $responseObj->getBody();
            $jsonResponseData = json_decode($data, false);
            $result = new \stdClass();
            // return response data as json if possible, raw if not
            $result->http_response_body = $data && $jsonResponseData === null ? $data : $jsonResponseData;

        } elseif ($httpResponseCode == 400) {

            throw new MissingRequiredParameters(ExceptionMessages::EXCEPTION_MISSING_REQUIRED_PARAMETERS . $this->getResponseExceptionMessage($responseObj));

        } elseif ($httpResponseCode == 422) {

            throw new FormValidationError(ExceptionMessages::EXCEPTION_FORM_VALIDATION . $this->getResponseExceptionMessage($responseObj));

        } elseif ($httpResponseCode == 401) {

            throw new InvalidCredentials(ExceptionMessages::EXCEPTION_INVALID_CREDENTIALS);

        } elseif ($httpResponseCode == 403) {

            throw new Unauthorized(ExceptionMessages::EXCEPTION_UNAUTHORIZED);
        } elseif ($httpResponseCode == 404) {

            throw new MissingEndpoint(ExceptionMessages::EXCEPTION_MISSING_ENDPOINT . $this->getResponseExceptionMessage($responseObj));
        } else {

            throw new GenericHTTPError(ExceptionMessages::EXCEPTION_GENERIC_HTTP_ERROR, $httpResponseCode, $responseObj->getBody());
        }

        $result->http_response_code = $httpResponseCode;

        return $result->http_response_body;
    }

    protected function getResponseExceptionMessage(ResponseInterface $responseObj)
    {
        $body = (string) $responseObj->getBody();
        $response = json_decode($body);
        if (json_last_error() == JSON_ERROR_NONE && isset($response->message)) {
            return ' ' . $response->message;
        }
    }

}
