<?php

namespace Orbis\Misc;

use Orbis\Connection\RestClient;

/**
 * Base
 */
abstract class Base
{
    public static function restClient()
    {
        return new RestClient();
    }
}
