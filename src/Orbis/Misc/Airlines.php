<?php

namespace Orbis\Misc;

class Airlines extends Base
{
    public static function list()
    {
        return self::restClient()->get('misc/airlines');
    }

    public static function show($iataCode)
    {
        return self::restClient()->get("misc/airlines/$iataCode");
    }
}
