<?php

namespace Orbis\Account;

use Orbis\Connection\RestClient;

/**
 * Base
 */
abstract class Base
{

    public static function restClient()
    {
        return new RestClient();
    }

}
