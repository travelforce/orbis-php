<?php

namespace Orbis\Account\PersonalAccounts\Resources;

use Orbis\Account\Auth;
use Orbis\Account\Base;

class LoyaltyCards extends Base
{
    public static function list($id, $page = '')
    {
        Auth::check();

        return self::restClient()->get("accounts/personal-accounts/$id/loyalty-cards", ['page' => $page]);
    }

    public static function create($id, $data)
    {
        Auth::check();

        return self::restClient()->post("accounts/personal-accounts/$id/loyalty-cards", $data);
    }

    public static function show($id, $cardId)
    {
        Auth::check();

        return self::restClient()->get("accounts/personal-accounts/$id/loyalty-cards/$cardId");
    }

    public static function update($id, $cardId, $data)
    {
        Auth::check();

        return self::restClient()->put("accounts/personal-accounts/$id/loyalty-cards/$cardId", $data);
    }

    public static function delete($id, $cardId)
    {
        Auth::check();

        return self::restClient()->delete("accounts/personal-accounts/$id/loyalty-cards/$cardId");
    }
}
