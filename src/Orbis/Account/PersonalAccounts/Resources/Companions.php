<?php

namespace Orbis\Account\PersonalAccounts\Resources;

use Orbis\Account\Auth;
use Orbis\Account\Base;

class Companions extends Base
{
    public static function list($id, $page = '')
    {
        Auth::check();

        return self::restClient()->get("accounts/personal-accounts/$id/companions", ['page' => $page]);
    }

    public static function create($id, $data)
    {
        Auth::check();

        return self::restClient()->post("accounts/personal-accounts/$id/companions", $data);
    }

    public static function show($id, $companionId)
    {
        Auth::check();

        return self::restClient()->get("accounts/personal-accounts/$id/companions/$companionId");
    }

    public static function update($id, $companionId, $data)
    {
        Auth::check();

        return self::restClient()->put("accounts/personal-accounts/$id/companions/$companionId", $data);
    }

    public static function delete($id, $companionId)
    {
        Auth::check();

        return self::restClient()->delete("accounts/personal-accounts/$id/companions/$companionId");
    }
}
