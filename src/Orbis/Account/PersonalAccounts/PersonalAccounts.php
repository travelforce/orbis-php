<?php

namespace Orbis\Account\PersonalAccounts;

use Orbis\Account\Auth;
use Orbis\Account\Base;

class PersonalAccounts extends Base
{
    public static function list($page = '')
    {
        Auth::check();

        return self::restClient()->get('accounts/personal-accounts', ['page' => $page]);
    }

    public static function create($data)
    {
        Auth::check();

        return self::restClient()->post('accounts/personal-accounts', $data);
    }

    public static function show($id)
    {
        Auth::check();

        return self::restClient()->get("accounts/personal-accounts/$id");
    }

    public static function update($id, $data)
    {
        Auth::check();

        return self::restClient()->put("accounts/personal-accounts/$id", $data);
    }

    public static function delete($id)
    {
        Auth::check();

        return self::restClient()->delete("accounts/personal-accounts/$id");
    }

    public static function attachBooking($id, $data)
    {
        Auth::check();

        return self::restClient()->post("accounts/personal-accounts/$id/attach-booking", $data);
    }

    public static function detachBooking($id, $data)
    {
        Auth::check();

        return self::restClient()->post("accounts/personal-accounts/$id/detach-booking", $data);
    }

    public static function bookings($id)
    {
        Auth::check();

        return self::restClient()->get("accounts/personal-accounts/$id/bookings");
    }

    public static function bookingStatistics($id)
    {
        Auth::check();

        return self::restClient()->get("accounts/personal-accounts/$id/booking-statistics");
    }

    public static function resetPassword($id)
    {
        Auth::check();

        return self::restClient()->post("accounts/personal-accounts/$id/reset-password");
    }
}
