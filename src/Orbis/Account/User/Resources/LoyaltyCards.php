<?php

namespace Orbis\Account\User\Resources;

use Orbis\Account\Auth;
use Orbis\Account\Base;

class LoyaltyCards extends Base
{
    public static function list($page = '')
    {
        Auth::check();

        return self::restClient()->get("accounts/user/loyalty-cards", ['page' => $page]);
    }

    public static function create($data)
    {
        Auth::check();

        return self::restClient()->post("accounts/user/loyalty-cards", $data);
    }

    public static function show($cardId)
    {
        Auth::check();

        return self::restClient()->get("accounts/user/loyalty-cards/$cardId");
    }

    public static function update($cardId, $data)
    {
        Auth::check();

        return self::restClient()->put("accounts/user/loyalty-cards/$cardId", $data);
    }

    public static function delete($cardId)
    {
        Auth::check();

        return self::restClient()->delete("accounts/user/loyalty-cards/$cardId");
    }
}
