<?php

namespace Orbis\Account\User\Resources;

use Orbis\Account\Auth;
use Orbis\Account\Base;

class Companions extends Base
{
    public static function list($page = '')
    {
        Auth::check();

        return self::restClient()->get("accounts/user/companions", ['page' => $page]);
    }

    public static function create($data)
    {
        Auth::check();

        return self::restClient()->post("accounts/user/companions", $data);
    }

    public static function show($companionId)
    {
        Auth::check();

        return self::restClient()->get("accounts/user/companions/$companionId");
    }

    public static function update($companionId, $data)
    {
        Auth::check();

        return self::restClient()->put("accounts/user/companions/$companionId", $data);
    }

    public static function delete($companionId)
    {
        Auth::check();

        return self::restClient()->delete("accounts/user/companions/$companionId");
    }
}
