<?php

namespace Orbis\Account\User;

use Orbis\Account\Auth;
use Orbis\Account\Base;

class User extends Base
{
    public static function show()
    {
        Auth::check();

        return self::restClient()->get('accounts/user');
    }

    public static function update($data)
    {
        Auth::check();

        return self::restClient()->put('accounts/user', $data);
    }

    public static function bookings()
    {
        Auth::check();

        return self::restClient()->get('accounts/user/bookings');
    }
}
