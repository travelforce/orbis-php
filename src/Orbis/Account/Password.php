<?php

namespace Orbis\Account;

class Password extends Base
{
    public static function reset($data)
    {
        return self::restClient()->post('accounts/reset-password', $data);
    }

    public static function setNew($code, $data)
    {
        return self::restClient()->post('accounts/reset-password/' . $code, $data);
    }
}
