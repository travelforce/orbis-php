<?php

namespace Orbis\Account;

class Activation extends Base
{
    public static function withCode($code)
    {
        return self::restClient()->post("accounts/activate-account/$code");
    }
}
