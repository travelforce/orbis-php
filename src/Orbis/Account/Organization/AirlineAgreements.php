<?php

namespace Orbis\Account\Organization;

use Orbis\Account\Auth;
use Orbis\Account\Base;

class AirlineAgreements extends Base
{
    public static function list($page = '')
    {
        Auth::check();

        return self::restClient()->get('accounts/organization/airline-agreements', ['page' => $page]);
    }

    public static function create($data)
    {
        Auth::check();

        return self::restClient()->post('accounts/organization/airline-agreements', $data);
    }

    public static function show($id)
    {
        Auth::check();

        return self::restClient()->get("accounts/organization/airline-agreements/$id");
    }

    public static function update($id, $data)
    {
        Auth::check();

        return self::restClient()->put("accounts/organization/airline-agreements/$id", $data);
    }

    public static function delete($id)
    {
        Auth::check();

        return self::restClient()->delete("accounts/organization/airline-agreements/$id");
    }
}
