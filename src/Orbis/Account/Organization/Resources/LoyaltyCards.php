<?php

namespace Orbis\Account\Organization\Resources;

use Orbis\Account\Auth;
use Orbis\Account\Base;

class LoyaltyCards extends Base
{
    public static function list($userId, $page = '')
    {
        Auth::check();

        return self::restClient()->get("accounts/organization/users/$userId/loyalty-cards", ['page' => $page]);
    }

    public static function create($userId, $data)
    {
        Auth::check();

        return self::restClient()->post("accounts/organization/users/$userId/loyalty-cards", $data);
    }

    public static function show($userId, $cardId)
    {
        Auth::check();

        return self::restClient()->get("accounts/organization/users/$userId/loyalty-cards/$cardId");
    }

    public static function update($userId, $cardId, $data)
    {
        Auth::check();

        return self::restClient()->put("accounts/organization/users/$userId/loyalty-cards/$cardId", $data);
    }

    public static function delete($userId, $cardId)
    {
        Auth::check();

        return self::restClient()->delete("accounts/organization/users/$userId/loyalty-cards/$cardId");
    }
}
