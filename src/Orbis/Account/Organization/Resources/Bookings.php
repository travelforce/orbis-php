<?php

namespace Orbis\Account\Organization\Resources;

use Orbis\Account\Auth;
use Orbis\Account\Base;

class Bookings extends Base
{
    public static function list($userId, $page = '')
    {
        Auth::check();

        return self::restClient()->get("accounts/organization/users/$userId/bookings", ['page' => $page]);
    }

    public static function show($userId, $bookingId)
    {
        Auth::check();

        return self::restClient()->get("accounts/organization/users/$userId/bookings/$bookingId");
    }
}
