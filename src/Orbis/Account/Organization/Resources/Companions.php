<?php

namespace Orbis\Account\Organization\Resources;

use Orbis\Account\Auth;
use Orbis\Account\Base;

class Companions extends Base
{
    public static function list($userId, $page = '')
    {
        Auth::check();

        return self::restClient()->get("accounts/organization/users/$userId/companions", ['page' => $page]);
    }

    public static function create($userId, $data)
    {
        Auth::check();

        return self::restClient()->post("accounts/organization/users/$userId/companions", $data);
    }

    public static function show($userId, $companionId)
    {
        Auth::check();

        return self::restClient()->get("accounts/organization/users/$userId/companions/$companionId");
    }

    public static function update($userId, $companionId, $data)
    {
        Auth::check();

        return self::restClient()->put("accounts/organization/users/$userId/companions/$companionId", $data);
    }

    public static function delete($userId, $companionId)
    {
        Auth::check();

        return self::restClient()->delete("accounts/organization/users/$userId/companions/$companionId");
    }
}
