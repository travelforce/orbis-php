<?php

namespace Orbis\Account\Organization;

use Orbis\Account\Auth;
use Orbis\Account\Base;

class Organization extends Base
{
    public static function show()
    {
        Auth::check();

        return self::restClient()->get('accounts/organization');
    }

    public static function update($data)
    {
        Auth::check();

        return self::restClient()->put('accounts/organization', $data);
    }

    public static function bookings()
    {
        Auth::check();

        return self::restClient()->get('accounts/organization/bookings');
    }

    public static function bookingStatistics()
    {
        Auth::check();

        return self::restClient()->get('accounts/organization/booking-statistics');
    }
}
