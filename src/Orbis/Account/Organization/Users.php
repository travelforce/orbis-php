<?php

namespace Orbis\Account\Organization;

use Orbis\Account\Auth;
use Orbis\Account\Base;

class Users extends Base
{
    public static function list($page = '')
    {
        Auth::check();

        return self::restClient()->get('accounts/organization/users', ['page' => $page]);
    }

    public static function create($data)
    {
        Auth::check();

        return self::restClient()->post('accounts/organization/users', $data);
    }

    public static function show($id)
    {
        Auth::check();

        return self::restClient()->get('accounts/organization/users/' . $id);
    }

    public static function update($id, $data)
    {
        Auth::check();

        return self::restClient()->put('accounts/organization/users/' . $id, $data);
    }

    public static function delete($id)
    {
        Auth::check();

        return self::restClient()->delete('accounts/organization/users/' . $id);
    }

    public static function resetPassword($id)
    {
        Auth::check();

        return self::restClient()->post('accounts/organization/users/' . $id . '/reset-password');
    }

    public static function updateRole($id, $data)
    {
        Auth::check();

        return self::restClient()->post('accounts/organization/users/' . $id . '/update-role', $data);
    }

    public static function bookings($id)
    {
        Auth::check();

        return self::restClient()->get('accounts/organization/users/' . $id . '/bookings');
    }

    public static function attachBooking($id, $data)
    {
        Auth::check();

        return self::restClient()->post('accounts/organization/users/' . $id . '/attach-booking', $data);
    }

    public static function detachBooking($id, $data)
    {
        Auth::check();

        return self::restClient()->post('accounts/organization/users/' . $id . '/detach-booking', $data);
    }
}
