<?php

namespace Orbis\Account\Organizations;

use Orbis\Account\Auth;
use Orbis\Account\Base;

class Users extends Base
{
    public static function list($id, $page = '')
    {
        Auth::check();

        return self::restClient()->get("accounts/organizations/$id/users", ['page' => $page]);
    }

    public static function create($id, $data)
    {
        Auth::check();

        return self::restClient()->post("accounts/organizations/$id/users", $data);
    }

    public static function show($id, $userId)
    {
        Auth::check();

        return self::restClient()->get("accounts/organizations/$id/users/$userId");
    }

    public static function update($id, $userId, $data)
    {
        Auth::check();

        return self::restClient()->put("accounts/organizations/$id/users/$userId", $data);
    }

    public static function delete($id)
    {
        Auth::check();

        return self::restClient()->delete("accounts/organizations/$id/users/$userId");
    }

    public static function resetPassword($id, $userId)
    {
        Auth::check();

        return self::restClient()->post("accounts/organizations/$id/users/$userId/reset-password");
    }

    public static function updateRole($id, $userId, $data)
    {
        Auth::check();

        return self::restClient()->post("accounts/organizations/$id/users/$userId/update-role", $data);
    }

    public static function bookings($id, $userId)
    {
        Auth::check();

        return self::restClient()->get("accounts/organizations/$id/users/$userId/bookings");
    }

    public static function attachBooking($id, $userId, $data)
    {
        Auth::check();

        return self::restClient()->post("accounts/organizations/$id/users/$userId/attach-booking", $data);
    }

    public static function detachBooking($id, $userId, $data)
    {
        Auth::check();

        return self::restClient()->post("accounts/organizations/$id/users/$userId/detach-booking", $data);
    }
}
