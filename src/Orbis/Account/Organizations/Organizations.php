<?php

namespace Orbis\Account\Organizations;

use Orbis\Account\Auth;
use Orbis\Account\Base;

class Organizations extends Base
{
    public static function list($page = '')
    {
        Auth::check();

        return self::restClient()->get('accounts/organizations', ['page' => $page]);
    }

    public static function create($data)
    {
        Auth::check();

        return self::restClient()->post('accounts/organizations', $data);
    }

    public static function show($id)
    {
        Auth::check();

        return self::restClient()->get("accounts/organizations/$id");
    }

    public static function update($id, $data)
    {
        Auth::check();

        return self::restClient()->put("accounts/organizations/$id", $data);
    }

    public static function delete($id)
    {
        Auth::check();

        return self::restClient()->delete("accounts/organizations/$id");
    }

    public static function bookings($id)
    {
        Auth::check();

        return self::restClient()->get("accounts/organizations/$id/bookings");
    }
}
