<?php

namespace Orbis\Account\Organizations\Resources;

use Orbis\Account\Auth;
use Orbis\Account\Base;

class Companions extends Base
{
    public static function list($id, $userId, $page = '')
    {
        Auth::check();

        return self::restClient()->get("accounts/organizations/$id/users/$userId/companions", ['page' => $page]);
    }

    public static function create($id, $userId, $data)
    {
        Auth::check();

        return self::restClient()->post("accounts/organizations/$id/users/$userId/companions", $data);
    }

    public static function show($id, $userId, $companionId)
    {
        Auth::check();

        return self::restClient()->get("accounts/organizations/$id/users/$userId/companions/$companionId");
    }

    public static function update($id, $userId, $companionId, $data)
    {
        Auth::check();

        return self::restClient()->put("accounts/organizations/$id/users/$userId/companions/$companionId", $data);
    }

    public static function delete($id, $userId, $companionId)
    {
        Auth::check();

        return self::restClient()->delete("accounts/organizations/$id/users/$userId/companions/$companionId");
    }
}
