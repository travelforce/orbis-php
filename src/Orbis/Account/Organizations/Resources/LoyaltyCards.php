<?php

namespace Orbis\Account\Organizations\Resources;

use Orbis\Account\Auth;
use Orbis\Account\Base;

class LoyaltyCards extends Base
{
    public static function list($id, $userId, $page = '')
    {
        Auth::check();

        return self::restClient()->get("accounts/organizations/$id/users/$userId/loyalty-cards", ['page' => $page]);
    }

    public static function create($id, $userId, $data)
    {
        Auth::check();

        return self::restClient()->post("accounts/organizations/$id/users/$userId/loyalty-cards", $data);
    }

    public static function show($id, $userId, $cardId)
    {
        Auth::check();

        return self::restClient()->get("accounts/organizations/$id/users/$userId/loyalty-cards/$cardId");
    }

    public static function update($id, $userId, $cardId, $data)
    {
        Auth::check();

        return self::restClient()->put("accounts/organizations/$id/users/$userId/loyalty-cards/$cardId", $data);
    }

    public static function delete($id, $userId, $cardId)
    {
        Auth::check();

        return self::restClient()->delete("accounts/organizations/$id/users/$userId/loyalty-cards/$cardId");
    }
}
