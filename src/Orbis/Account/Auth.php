<?php

namespace Orbis\Account;

/**
 * Auth class
 */
class Auth extends Base
{
    // Login details
    private static $email;

    private static $password;

    public static function auth()
    {
        if (!(self::$email && self::$password)) {
            throw new \Exception('Login credentials not provided');

            return;
        }
        $result = self::restClient()->post('accounts/auth', ['email' => self::$email, 'password' => self::$password]);

        return self::setAuthToken($result->token);

    }

    public static function check()
    {
        if (self::getAuthToken()) {

            return true;
        } else if (self::auth()) {

            return true;
        }

        return false;
    }

    public static function setCredentials($email, $password)
    {
        self::$email = $email;
        self::$password = $password;
        self::auth();
    }

    public static function setEmail($email)
    {
        self::$email = $email;

        return;
    }

    public static function getEmail()
    {
        return self::$email;
    }

    public static function setPassword($password)
    {
        self::$password = $password;

        return;
    }

    public static function setAuthToken($token)
    {
        $_SESSION['auth_token'] = $token;

        return true;

    }

    public static function getAuthToken()
    {

        return @$_SESSION['auth_token'];
    }

    public static function logout()
    {
        unset($_SESSION['auth_token']);

        return true;
    }
}
